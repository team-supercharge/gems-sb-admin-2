# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'sb_admin_2/rails/version'

Gem::Specification.new do |spec|
  spec.name          = "sb_admin_2-rails"
  spec.version       = SbAdmin2::Rails::VERSION
  spec.authors       = ["Adam Schwartz"]
  spec.email         = ["adam.schwartz@jbslabs.com"]

  spec.summary       = "SB Admin 2 template for Rails4"
  spec.description   = "This gem provides asset pipeline integration for SB Admin 2"
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.8"
  spec.add_development_dependency "railties", "~> 4.1"
  spec.add_development_dependency "rake", "~> 10.0"
end
